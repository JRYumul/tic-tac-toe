let row1 = []
let row2 = []
let row3 = []
let row4 = []
let row5 = []
let row6 = []
let row7 = []
let row8 = []
let all = []

let turn = document.getElementById("turn")
let cell = document.getElementsByClassName("cell")

function winCheck(){
    if(((row1[0] == "o") && (row1[1] == "o") && (row1[2] == "o"))
    || ((row2[0] == "o") && (row2[1] == "o") && (row2[2] == "o"))
    || ((row3[0] == "o") && (row3[1] == "o") && (row3[2] == "o"))
    || ((row4[0] == "o") && (row4[1] == "o") && (row4[2] == "o"))
    || ((row5[0] == "o") && (row5[1] == "o") && (row5[2] == "o"))
    || ((row6[0] == "o") && (row6[1] == "o") && (row6[2] == "o"))
    || ((row7[0] == "o") && (row7[1] == "o") && (row7[2] == "o"))
    || ((row8[0] == "o") && (row8[1] == "o") && (row8[2] == "o"))){
    turn.innerHTML = "O Wins!"
    confetti.start();
    }else if(((row1[0] == "x") && (row1[1] == "x") && (row1[2] == "x"))
    || ((row2[0] == "x") && (row2[1] == "x") && (row2[2] == "x"))
    || ((row3[0] == "x") && (row3[1] == "x") && (row3[2] == "x"))
    || ((row4[0] == "x") && (row4[1] == "x") && (row4[2] == "x"))
    || ((row5[0] == "x") && (row5[1] == "x") && (row5[2] == "x"))
    || ((row6[0] == "x") && (row6[1] == "x") && (row6[2] == "x"))
    || ((row7[0] == "x") && (row7[1] == "x") && (row7[2] == "x"))
    || ((row8[0] == "x") && (row8[1] == "x") && (row8[2] == "x"))){
    turn.innerHTML = "X Wins!"
    confetti.start();
    }else if(all.length > 8){
    turn.innerHTML = "Draw..." 
    }
}

let makeO = function(e){
    if((e.target.innerHTML == "X") || (e.target.innerHTML == "O")){
        alert("error")
    }else{
        e.target.innerHTML = "O"
        turn.innerHTML = "Current turn: X"
        if(e.target.id == "a1"){
            row1.push("o")
            row4.push("o")
            row7.push("o")
            all.push("o")
        }else if(e.target.id == "a2"){
            row1.push("o")
            row5.push("o")
            all.push("o")
        }else if(e.target.id == "a3"){
            row1.push("o")
            row6.push("o")
            row8.push("o")
            all.push("o")
        }else if(e.target.id == "b1"){
            row2.push("o")
            row4.push("o")
            all.push("o")
        }else if(e.target.id == "b2"){
            row2.push("o")
            row5.push("o")
            row7.push("o")
            row8.push("o")
            all.push("o")
        }else if(e.target.id == "b3"){
            row2.push("o")
            row6.push("o")
            all.push("o")
        }else if(e.target.id == "c1"){
            row3.push("o")
            row4.push("o")
            row8.push("o")
            all.push("o")
        }else if(e.target.id == "c2"){
            row3.push("o")
            row5.push("o")
            all.push("o")
        }else if(e.target.id == "c3"){
            row3.push("o")
            row6.push("o")
            row7.push("o")
            all.push("o")
        }
        winCheck()
        clickX()
    }
}
let makeX = function(e){
    if((e.target.innerHTML == "X") || (e.target.innerHTML == "O")){
        alert("error")
    }else{
        e.target.innerHTML = "X"
        turn.innerHTML = "Current turn: O"
        if(e.target.id == "a1"){
            row1.push("x")
            row4.push("x")
            row7.push("x")
            all.push("x")
        }else if(e.target.id == "a2"){
            row1.push("x")
            row5.push("x")
            all.push("x")
        }else if(e.target.id == "a3"){
            row1.push("x")
            row6.push("x")
            row8.push("x")
            all.push("x")
        }else if(e.target.id == "b1"){
            row2.push("x")
            row4.push("x")
            all.push("x")
        }else if(e.target.id == "b2"){
            row2.push("x")
            row5.push("x")
            row7.push("x")
            row8.push("x")
            all.push("x")
        }else if(e.target.id == "b3"){
            row2.push("x")
            row6.push("x")
            all.push("x")
        }else if(e.target.id == "c1"){
            row3.push("x")
            row4.push("x")
            row8.push("x")
            all.push("x")
        }else if(e.target.id == "c2"){
            row3.push("x")
            row5.push("x")
            all.push("x")
        }else if(e.target.id == "c3"){
            row3.push("x")
            row6.push("x")
            row7.push("x")
            all.push("x")
        }
        winCheck()
        clickO()
    }
}

function clickO(){
    for(let i = 0; i < cell.length; i++){
        cell[i].removeEventListener("click", makeX)
        cell[i].addEventListener("click", makeO)
        }
}

clickO()

function clickX(){
    for(let i = 0; i < cell.length; i++){
        cell[i].removeEventListener("click", makeO)
        cell[i].addEventListener("click", makeX)
        }
}